import {readFileSync, writeFileSync, existsSync} from 'fs';
import {parse} from './parser';
import {Entry, makeTemplate, entryToString} from './Entry';

export class EntryDB {
    private fileName: string = 'work.jrnl';
    private entries: Array<Entry> = [];
    private nextId: number = 1;

    constructor(fileName?: string) {
        if (fileName) {
            this.fileName = fileName;
        }
    }

    public init(): void {
        if (existsSync(this.fileName)) {
            const fileContents = readFileSync(this.fileName, 'utf8');
            this.entries = parse(fileContents)
                .filter((e: Entry) => e.subject !== 'Template');
            this.sort();
            this.assignIds();
        } else {
            this.writeTemplate();
        }
    }

    public findById(id: string): Entry | undefined {
        return this.entries.find(e => e.id === id);
    }

    public allEntries(): Array<Entry> {
        return this.entries;
    }

    public import(entries: Array<Entry>): void {
        entries.forEach(e => this._save(e));
        this.flush();
    }

    public save(entry: Entry): Entry {
        const updatedEntry = this._save(entry);
        this.flush();

        return updatedEntry;
    }

    public delete(id: string): void {
        this.entries = this.entries.filter(e => e.id !== id);
        this.flush();
    }

    private _save(entry: Entry): Entry {
        if (entry.id) {
            this.entries = this.entries.map(e => {
                if (e.id === entry.id) {
                    return entry;
                }
                return e;
            });
        } else {
            entry.id = this.genId();
            for (let i = 0; i < this.entries.length; i++) {
                if (entry.date > this.entries[i].date) {
                    this.entries.splice(1, 0, entry)
                    break;
                }
            }
        }

        return entry;
    }

    private flush(): void {
        const data = makeTemplate() + '\n\n' + this.entries.map(e => entryToString(e)).join('\n\n');
        writeFileSync(this.fileName, data);
    }

    private sort(): void {
        this.entries.sort((a: Entry, b: Entry) => {
            if ((a.date) < (b.date)) return 1;
            if ((a.date) > (b.date)) return -1;
            return 0;
        });
    }

    private writeTemplate(): void {
        writeFileSync(this.fileName, makeTemplate());
    }

    private assignIds(): void {
        this.entries.forEach(e => e.id = this.genId());
    }

    private genId(): string {
        return (this.nextId++).toString();
    }
}
