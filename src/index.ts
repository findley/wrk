import * as fs from 'fs';
import * as path from 'path';
import * as ini from 'ini';
import * as os from 'os';
import { Command } from 'commander';
import * as chokidar from 'chokidar';
import {parse} from './parser';
import {makeTemplate} from './Entry';

const defaultConfig: Config = {
    editor: 'vim',
}

const program = new Command();
program
    .version('0.1.0')
    .arguments('[jrnlfile]')
    .action(openJournal)
    .parse(process.argv);

async function openJournal(argFile: string): Promise<void> {
    const config = loadConfig(argFile);
    const file = argFile ?? config.default;

    if (file === undefined || file === null) {
        console.error('A file was not provided and no default is set in ~/.jrnlconfig', file);
        process.exit(1);
    }

    if (!fileExists(file)) {
        initJournal(file);
    }

    // Start watching for changes in file.
    let jrnlValid = true;
    let lastErr: any = null;
    const watcher = chokidar.watch(file);
    watcher.on('change', () => {
        try {
            parseJournal(file);
        } catch (err) {
            lastErr = err;
            jrnlValid = false;
            return;
        }

        jrnlValid = true;
    });

    //const editorProcess = child.spawn(editor, [file], { stdio: 'inherit' });
    //editorProcess.on('close', () => {
    //    if (!jrnlValid) {
    //        const line = lastErr.location.start.line;
    //        const col = lastErr.location.start.column;
    //        console.error(`Your jrnl file has a syntax error at line ${line} column ${col}.`);
    //    }
    //    watcher.close();
    //});
}

function fileExists(file: string): boolean {
    try {
        fs.accessSync(file);
    } catch(err) {
        return false;
    }

    return true;
}

function initJournal(file: string): void {
    fs.writeFileSync(file, makeTemplate());
}

function parseJournal(file: string): any {
    const fileContents = fs.readFileSync(file, 'utf8');
    const e = parse(fileContents);
    return e;
}

function loadConfig(jrnlFile: string|null): Config {
    const configPath = path.join(os.homedir(), '.jrnlconfig');
    if (!fileExists(configPath)) {
        if (jrnlFile === undefined) {
            return defaultConfig;
        }

        const configContent = ini.encode({...defaultConfig, default: jrnlFile});
        fs.writeFileSync(configPath, configContent);
    }

    const fileContents = fs.readFileSync(configPath, 'utf8');
    const config = ini.parse(fileContents);
    // TODO: Validate config?

    console.log('config:', config);
    return config;
}

type Config = {
    default?: string;
    editor?: string;
}
