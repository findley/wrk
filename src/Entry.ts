import moment from 'moment';

export type Entry = {
    subject: string,
    body: string,
    date: string,
    duration: string,
    id?: string
}

export function entryToString (entry: Entry): string {
    let duration = '';
    if (entry.duration) {
        duration = `\nDuration: ${entry.duration}`
    }

    const paras = entry.body.split('\n\n');
    const formattedParas = paras.map(text => {
        const words = text.replace(/\n/g, ' ').split(' ');
        let lines = [];
        let line: Array<string> = [];
        words.forEach(word => {
            const currentLen = line.reduce((t, w) => t + w.length, 0) + line.length - 1;
            if (currentLen + word.length <= 75) {
                line.push(word);
            } else {
                lines.push('    ' + line.join(' '));
                line = [word];
            }
        });
        lines.push('    ' + line.join(' '));
        return lines.join('\n');
    });
    const body = formattedParas.join('\n\n');

    return `Date: ${entry.date}
Subject: ${entry.subject}${duration}

${body}
`;
}

export const makeTemplate = () => {
    const date = moment().format('YYYY-MM-DD');

    return `Date: ${date}
Subject: Template
Duration: 1h30m

    Write some stuff here. You can mention people with @person, you can tag
    entries with #hashtags, and you can link issue numbers with !ISSUE-12345

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fermentum
    suscipit convallis. Vivamus ut finibus urna, et pharetra sapien. Sed vitae
    diam in libero ultricies commodo ac et metus. Pellentesque a egestas libero.
    Vivamus nec erat sit amet dui aliquam molestie ut nec nunc. Vestibulum vel
    auctor metus, et auctor metus.
`
};
