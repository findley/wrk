import { Server } from './Server';
import { EntryDB } from './EntryDB';

const dataFile = process.argv[2] || 'my.jrnl';

const db = new EntryDB(dataFile);
db.init();

const server = new Server(3000, db);
server.start();
