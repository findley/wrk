"use strict";
exports.__esModule = true;
var moment_1 = require("moment");
function entryToString(entry) {
    var duration = '';
    if (entry.duration) {
        duration = "\nDuration: " + entry.duration;
    }
    var paras = entry.body.split('\n\n');
    var formattedParas = paras.map(function (text) {
        var words = text.replace(/\n/g, ' ').split(' ');
        var lines = [];
        var line = [];
        words.forEach(function (word) {
            var currentLen = line.reduce(function (t, w) { return t + w.length; }, 0) + line.length - 1;
            if (currentLen + word.length <= 75) {
                line.push(word);
            }
            else {
                lines.push('    ' + line.join(' '));
                line = [word];
            }
        });
        lines.push('    ' + line.join(' '));
        return lines.join('\n');
    });
    var body = formattedParas.join('\n\n');
    return "Date: " + entry.date + "\nSubject: " + entry.subject + duration + "\n\n" + body + "\n";
}
exports.entryToString = entryToString;
exports.makeTemplate = function () {
    var date = moment_1["default"]().format('YYYY-MM-DD');
    return "Date: " + date + "\nSubject: Template\nDuration: 1h30m\n\n    Write some stuff here. You can mention people with @person, you can tag\n    entries with #hashtags, and you can link issue numbers with !ISSUE-12345\n\n    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras fermentum\n    suscipit convallis. Vivamus ut finibus urna, et pharetra sapien. Sed vitae\n    diam in libero ultricies commodo ac et metus. Pellentesque a egestas libero.\n    Vivamus nec erat sit amet dui aliquam molestie ut nec nunc. Vestibulum vel\n    auctor metus, et auctor metus.\n";
};
