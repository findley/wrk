import { EntryDB } from './EntryDB';
import restify, { Response, Request, Next } from 'restify';
import corsMiddleware from 'restify-cors-middleware';

export class Server {
    private db: EntryDB;
    private port: number;
    private server: restify.Server;

    constructor(port: number, db: EntryDB) {
        this.port = port;
        this.db = db;
        this.server = restify.createServer();
        this.server.use(restify.plugins.bodyParser());
        this.setupCors();

        this.attachHandlers();
    }

    public start() {
        this.server.listen(this.port, () => {
            console.log(`Server running at ${this.port}`);
        });
    }

    private attachHandlers(): void {
        this.server.get('/api/entries', this.getEntries.bind(this));
        this.server.put('/api/entries/:id', this.saveEntry.bind(this));
        this.server.del('/api/entries/:id', this.deleteEntry.bind(this));
        this.server.get('/*', restify.plugins.serveStaticFiles('./client/build'));
    }

    private getEntries(req: Request, res: Response, next: Next) {
        res.send(this.db.allEntries());

        return next();
    }

    private saveEntry(req: Request, res: Response, next: Next) {
        const entry = this.db.save(req.body);
        res.send(entry);

        return next();
    }

    private deleteEntry(req: Request, res: Response, next: Next) {
        const id = req.params['id'];
        this.db.delete(id);

        return next();
    }

    private setupCors() {
        const cors = corsMiddleware({
            origins: ['*'],
            allowHeaders: [],
            exposeHeaders: []
        });

        this.server.pre(cors.preflight);
        this.server.use(cors.actual);
    }
}
