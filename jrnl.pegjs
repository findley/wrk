Document "doc"
    = Entry*

Entry "entry"
    = date:DateLine
      subject:SubjectLine
      duration:DurationLine?
      newline
      body:Body
      EOL
      {
        return {
        	subject: subject,
            date: date,
            duration: duration,
            body: body
        };
      }

DateLine "date-line"
    = "Date:"i _ d:Date _ EOL { return d; }

SubjectLine "subject-line"
    = "Subject:"i text:LineOfText { return text; }

DurationLine "duration-line"
    = "Duration:"i _ d:Duration _ EOL { return d; }

Body "body"
	= paras:Para+ { return paras.join('\n\n'); }

Duration "duration"
    = $([0-9]+ 'h' ([0-9]+ 'm')?)
    / $([0-9]+ 'm')

Date "date"
    = $([0-9]+ "-" [0-9]+  "-" [0-9]+)

Para "paragraph"
    = text:LineOfText+ EOL { return text.join(" "); }

LineOfText "line of text"
    = _ text:$(char+) EOL { return text; }

char = [^\r\n]
newline = '\n' / '\r' / '\r' '\n'?

EOL = newline /!.

_ "whitespace"
    = [ \t]*
