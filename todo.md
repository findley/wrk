# TODO
- building / packaging
- Do a better job of preserving formatting
- Implement metadata endpoint
- pagination of fetch entries
- Implement searching
- stable sorting of entries (lodash?)

- Think about how to prevent datalose (backups, tmp files, etc?)
- Consider implementing eventsourcing mechanism
  (maybe in dot file next to jrnl file?)
